from django.urls import path
from . import views


urlpatterns = [
    path('', views.listadoEquipo),
    path('guardarEquipo/', views.guardarEquipo),
    path('eliminarEquipo/<int:id_equi>/', views.eliminarEquipo),
    path('editarEquipo/<int:id_equi>/', views.editarEquipo),
    path('procesarActualizacionEquipo/', views.procesarActualizacionEquipo),
    #Posicion
    path('listadoPosicion/', views.listadoPosicion),
    path('guardarPosicion/', views.guardarPosicion),
    path('eliminarPosicion/<int:id_pos>/', views.eliminarPosicion),
    path('editarPosicion/<int:id_pos>/', views.editarPosicion),
    path('procesarActualizacionPosicion/', views.procesarActualizacionPosicion),
    #Jugador
    path('listadoJugador/', views.listadoJugador),
    path('guardarJugador/', views.guardarJugador),
    path('eliminarJugador/<int:id_jug>/', views.eliminarJugador),
    path('editarJugador/<int:id_jug>/', views.editarJugador),
    path('procesarActualizacionJugador/', views.procesarActualizacionJugador),

]